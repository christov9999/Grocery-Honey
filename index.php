<?php
	session_start();

	if (isset($_SESSION["username"])) {
		// connect to database and include functions
		include_once("db_functions.php");

		// store session variables to local variables
		$u_id = $_SESSION["u_id"];
		$username = $_SESSION["username"];

		// get user's lists
		try {
			$lists = db_select_all("
					SELECT * FROM `lists`
					WHERE u_id=$u_id
				");
		} catch (Exception $e) {
			$error = "A database error occurred.";
			$e_msg = $e -> getMessage();
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Grocery Honey</title>
</head>
<body>
	<div id="univ_wrapper">
		<?php
			if (isset($_SESSION["username"])) {
				// logged in, output user's lists
		?>
		<div id="listview_wrapper">
			<div id="listview_header">
				Welcome, <?php echo $username; ?>
			</div>
			<div id="listview_content">
				<?php
					if (count($lists) == 0) {
						// list is empty
				?>
				<div id="listview_empty">
					No grocery lists found.
					How about <a href="">making one</a>?
				</div>
				<?php
					} else {
						// list not empty, iterate through each list
						foreach ($lists as $list) {
				?>
				<div class="listview_list">
					<?php echo $list["name"]; ?>
				</div>
				<?php
						}
					}
				?>
			</div>
		</div>
		<?php
			} else {
				// not logged in, output introduction page and link to login
		?>
		<div id="intro_wrapper">
			Grocery Honey is a blah blah blah
			<br/>
			<a href="login.php">Log in</a> to view your lists.
		</div>
		<?php
			}
		?>
	</div>
</body>
</html>