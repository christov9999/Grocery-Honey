<?php
	session_start();

	if (isset($_SESSION["username"])) {
		// redirect to index.php if logged in
		header("Location: index.php");
		die("Redirecting...");
	}

	if (isset($_POST["loginform_username"])) {
		// POST values are present, user submitted login attempt

		// connect to database and include functions
		include_once("db_functions.php");

		try {
			// store POST variables to local variables
			$username = $_POST["loginform_username"];
			$password = $_POST["loginform_password"];

			// check if user and password match
			$row = db_select("SELECT * FROM `users` WHERE username='$username'");
			if ($row == null) {
				// user doesn't exists
				$error = "Username not found. Please <a href=\"register.php\">register</a> from this page.";
			} else {
				// user exists, verify password
				if (password_verify($password, $row["password"])) {
					// password is correct, set session variables
					$_SESSION["u_id"] = $row["u_id"];
					$_SESSION["username"] = $username;

					// redirect to index.php
					header("Location: login.php");
					die("Redirecting...");
				} else {
					// incorrect password
					$error = "Incorrect password. Please try again.";
				}
			}

			// otherwise output login form again with error (...)
		} catch (Exception $e) {
			$error = "A database error has occurred.";
			$e_msg = $e -> getMessage();
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
</head>
<body>
	<div id="univ_wrapper">
		<?php
			if (isset($error)) {
		?>
		<div class="err_msg">
			<?php echo $error; ?>
		</div>
		<?php
			}
		?>
		<form action="login.php" method="POST">
			<input type="text" name="loginform_username">
			<input type="password" name="loginform_password">
			<input type="submit" value="Log In">
		</form>
	</div>
</body>
</html>