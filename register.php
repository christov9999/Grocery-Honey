<?php
	session_start();

	if (isset($_SESSION["username"])) {
		// redirect to index.php if logged in
		header("Location: index.php");
		die("Redirecting...");
	}

	if (isset($_POST["registerform_username"])) {
		// POST values are present, user submitted register attempt

		// connect to database and include functions
		include_once("db_functions.php");

		try {
			// store POST variables to local variables
			$username = $_POST["registerform_username"];
			$password = $_POST["registerform_password"];

			// check if user already exists
			$row = db_select("SELECT * FROM `users` WHERE username='$username'");
			if ($row != null) {
				// user already exists
				$error = "Username already exists. Please choose another.";
			} else {
				// new user, add user to database
				// hash password for storing
				$password = password_hash($password, PASSWORD_DEFAULT);

				// insert to database
				db_insert("INSERT INTO `users`(username,password) VALUES('$username','$password')");

				// redirect to login.php
				header("Location: login.php");
				die("Redirecting...");
			}

			// otherwise output register form again with error (...)
		} catch (Exception $e) {
			$error = "A database error has occurred.";
			$e_msg = $e -> getMessage();
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Register</title>
</head>
<body>
	<div id="univ_wrapper">
		<?php
			if (isset($error)) {
		?>
		<div class="err_msg">
			<?php echo $error; ?>
		</div>
		<?php
			}
		?>
		<form action="register.php" method="POST">
			<input type="text" name="registerform_username">
			<input type="password" name="registerform_password">
			<input type="submit" value="Register">
		</form>
	</div>
</body>
</html>