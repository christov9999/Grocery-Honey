<?php
	$db_host = "localhost";
	$db_name = "grocery-honey";
	$db_user = "root";
	$db_pwd = "";

	try {
		$db = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_pwd);
		$connected = true;
	} catch(PDOException $e) {
	?>
		<div style="position: fixed; top: 0; left: 0; width: 100%; height: 100%;
					background: rgba(0,0,0,0.9); color: #fff; text-align: center;
					font-family: 'Open Sans', sans-serif;">
			<div style="position: relative; width: 100%; height: 50%; font-size: 34px; font-weight: 300;">
				<div style="position: absolute; bottom: 0; left: 0; width: 100%;">
					<div style="margin: 0 auto; padding: 0 0 15px 0; width: 80%; max-width: 960px; min-width: 480px;">
						There was an error connecting to the database.
					</div>
				</div>
			</div>
			<div style="position: relative; width: 100%; height: 50%; font-size: 14px;">
				<div style="position: absolute; top: 0; left: 0; width: 100%;">
					<div style="margin: 0 auto; padding: 10px 0 0 0; width: 80%; max-width: 960px; min-width: 480px;">
						<?php echo $e -> getMessage(); ?>
					</div>
				</div>
			</div>
		</div>
	<?php
		$connected = false;
	}

	if ($connected) {
		// database query functions

		// returns 1 result from query string
		function db_select($query) {
			global $db;
			try {
				$st = $db -> prepare($query);
				$st -> execute();
				return $st -> fetch(PDO::FETCH_ASSOC);
			} catch (Exception $e) {
				throw $e;
			}
		}

		// returns array of results from query string
		function db_select_all($query) {
			global $db;
			try {
				$st = $db -> prepare($query);
				$st -> execute();
				return $st -> fetchAll(PDO::FETCH_ASSOC);
			} catch (Exception $e) {
				throw $e;
			}
		}

		// returns insert id of inserted row
		function db_insert($query) {
			global $db;
			try {
				$db -> exec($query);
				return $db -> lastInsertId();
			} catch (Exception $e) {
				throw $e;
			}
		}

		// returns affected rows from update query
		function db_update($query) {
			global $db;
			try {
				return $db -> exec($query);
			} catch (Exception $e) {
				throw $e;
			}
		}

		// returns affected rows from delete query
		function db_delete($query) {
			global $db;
			try {
				return $db -> exec($query);
			} catch (Exception $e) {
				throw $e;
			}
		}
	}